<?php
namespace App\Controller ;
use App\Controller\AppController ;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security ;
use Cake\Validation\Validator;
class UsersController extends AppController
{
	public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash'); // Include the FlashComponent
        //$this->viewBuilder()->layout('Layout_Name');
    }
	public function index()
	{

		//$user = TableRegistry::get('users')->find();
		$user = $this->Users->find() ;
		$this->set(['users'=>$user]) ;
		//return $this->render('/user/index') ;
	}
	public function create()
	{
		$user = $this->Users->newEntity() ;

		if($this->request->is('post'))
		{
			$data = $this->request->getData() ;
		
			$data["created"] = date('Y-m-d H:i:s') ;
			$data["modified"] = date('Y-m-d H:i:s') ;
			//$data["password"] = Security::encrypt(null,$this->request->getData()["password"]) ;
		//	print_r($data) ;
			//echo $data["email"] ;
		//	$key = $this->request->getData()["password"] ;
		//	$result = Security::encrypt(null, $key);
			//echo $result ;
			//echo Security::encrypt(null,$key);
			//die;
			
			$user = $this->Users->patchEntity($user,$data) ;
			// if($user->errors()){
				
			// 	//print_r($user->errors()) ;
			// 	//die;
			// 	if($user->errors('email')){
					
			// 		foreach($user->errors('email') as $key => $value){
			// 			//echo $value ;
			// 			$this->Flash->error($value) ;
			// 		}

			// 		//$this->Flash->error($user->errors('email')) ;
			// 		//die;
			// 	}
			// 	if($user->errors('password')){
					
			// 		foreach($user->errors('password') as $key => $value){
			// 			$this->Flash->error($value) ;
			// 		}
			// 	}
			// }
		 	if($this->Users->save($user)){
				$this->Flash->success("User has been saved. ") ;
				return $this->redirect(['action'=>'index']) ;
			}
		 	//$this->Flash->error("Unable add user ") ;
			
		}
		// if($this->request->is('post')){
		// 	$validator = new Validator();
		// 	$validator
		// 	    ->requirePresence('email')
		// 	    ->add('email', 'validFormat', [
		// 	        'rule' => 'email',
		// 	        'message' => 'E-mail must be valid'
		// 	    ])
		// 	    ->requirePresence('password')
		// 	    ->notEmpty('password', 'We need your password.');
			    
		// 	$errors = $validator->errors($this->request->getData());
			
		// 	if (!empty($errors)) {
		// 		foreach($errors as $key => $value){
		// 			print_r($value) ;
		// 		}
		// 	    // Send an email.
		// 	}
		// 	die;
		// }
		
		$this->set(['user'=>$user]) ;
		//$this->render('/user/create') ;
	}
	public function edit($id){
		 $user = $this->Users->findById($id)->firstOrFail() ;
		if($this->request->is(['post','put']))
		{

			$data = [	'email' 	=> $this->request->getData()["email"],
						'modified'	=> date('Y-m-d H:i:s')
					] ;
			if(!empty($this->request->getData()["password"])){
				$data["password"] = $this->request->getData()["password"] ;
			}
			 
			$this->Users->patchEntity($user,$data) ;
			// if($user->errors()){
			// 	if($user->errors('email')){
			// 		foreach($user->errors('email') as $key => $value){
			// 			//echo $value ;
			// 			$this->Flash->error($value) ;
			// 		}
			// 	}
			// 	if($user->errors('password')){
			// 		foreach($user->errors('password') as $key => $value){
			// 			$this->Flash->error($value) ;
			// 		}
			// 	}
			// }
			if($this->Users->save($user))
			{
				$this->Flash->success("User has been updated") ;
				return $this->redirect(['action'=>'index']) ;
			}

			//$this->Flash->error("Unable update user ") ;
		}
		$this->set(['user'=>$user]) ;
		//$this->render('/user/edit') ;
	}
	public function delete($id)
	{
		$this->request->allowMethod(['post','delete']) ;
		$user = $this->Users->findById($id)->firstOrFail() ;
		if($this->Users->delete($user))
		{
			$this->Flash->success(__('The {0} user has been delete', $user->email)) ;
			return $this->redirect(['action' => 'index']) ;
		}
		
	}
}