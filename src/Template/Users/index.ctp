<h1>Users</h1>
<p><?= $this->Html->link("Add User", ['action' => 'create']) ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Email</th>
        <th>Action</th>
    </tr>

<!-- Here's where we iterate through our $articles query object, printing out article info -->

<?php foreach ($users as $use): ?>
    <tr>
        <td>
            <?= $use->id ;
            //$this->Html->link($use->id, ['action' => 'view', $use->id]) 
            ?>
        </td>
        <td>
            <?=  $use->email;
            
            ?>
        </td>
        <td>
            <?= $this->Html->link('Edit',['action'=>'edit',$use->id]) ; ?>
            <?= $this->Form->postLink(
                'Delete',
                ['action'=>'delete',$use->id],
                ['confirm' =>'Are you sure ?'])
            ?>
         
        </td>
    </tr>
<?php endforeach; ?>

</table>