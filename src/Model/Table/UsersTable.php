<?php
// in src/Model/Table/ArticlesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
// the Text class
use Cake\Utility\Text;
use Cake\Validation\Validator;
// Add the following method.
/*
public function beforeSave($event, $entity, $options)
{
    if ($entity->isNew() && !$entity->slug) {
        $sluggedTitle = Text::slug($entity->title);
        // trim slug to maximum length defined in schema
        $entity->slug = substr($sluggedTitle, 0, 191);
    }
}
*/
class UsersTable extends Table{
    public function validationDefault(Validator $validator)
    {
        // $validator
        //     ->requirePresence('email', 'create')
        //     ->notEmpty('email')
        //     ->minLength('password', 8)
        //     ->maxLength('password', 50) ;
        $validator
            ->notEmpty('email','Email not empty')
          //  ->requirePresence('password','create')
            ->notEmpty('password','Password not empty')
            ->add('email','valid',[
                'rule'      =>'email',
                'message'   =>  __('Valid email')
            ])
            ->add('password', 'length', 
                [   'rule' => ['lengthBetween', 8, 100],
                    'message' => __('Password beetwen 8 and 100 char')
                ]);
            
        return $validator;
    }
    public function validationUpdate($validator)
    {
        // $validator = $this->validationDefault($validator);
        // $validator
        //    ->add('email','notEmpty',[
        //         'rule' => 'notEmpty',
        //         'message' => 'Email not empty'
        //     ]) ;
        // return $validator;
    }

}
